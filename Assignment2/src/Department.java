import java.util.Set;

public class Department {
	private String DepartmentId;
	private String DepartmentName;
	private Set Employees;

	public Department() {}
	
	public Department(String departmentId, String departmentName) {
		super();
		DepartmentId = departmentId;
		DepartmentName = departmentName;
	}
	public String getDepartmentId() {
		return DepartmentId;
	}
	public void setDepartmentId(String departmentId) {
		DepartmentId = departmentId;
	}
	public String getDepartmentName() {
		return DepartmentName;
	}
	public void setDepartmentName(String departmentName) {
		DepartmentName = departmentName;
	}
	public Set getEmployees() {
		return Employees;
	}
	public void setEmployees(Set employees) {
		Employees = employees;
	}
	
	
	
}
