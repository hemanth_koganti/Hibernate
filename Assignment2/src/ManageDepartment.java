
import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManageDepartment {

	private static SessionFactory factory;
	public static void main(String[] args) {
		
		try{
			factory = new Configuration().configure().buildSessionFactory();
		} catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		
		ManageDepartment MD = new ManageDepartment();
		
		HashSet set1 = new HashSet();
		set1.add(new Employee("he388719","Hemanth","B1","ABC"));
		set1.add(new Employee("ma678543","Manoj","B3","ABC"));
		set1.add(new Employee("ra345678","Ravi","B2","ABC"));
		
		String Dep1 = MD.addDepartment("ABC","abcdefgh",set1);
		
		HashSet set2 = new HashSet();
		set2.add(new Employee("ro456378","Rohith","C1","JKL"));
		set2.add(new Employee("sa345890","Sai Kumar","C2","JKL"));
		
		String Dep2 = MD.addDepartment("JKL","jklmnop",set2);
		
		MD.listDepartments();
		
		MD.updateDepartment(Dep1,"abcde");
		
		MD.deleteDepartment(Dep2);
		
		MD.listDepartments();
				
	}
	
	public String addDepartment(String depno,String depname,Set emps){
		Session session = factory.openSession();
		Transaction tx = null;
		String departmentID = null;
		try{
			tx = session.beginTransaction();
			Department department = new Department(depno,depname);
			department.setEmployees(emps);
			departmentID = (String) session.save(department);
			tx.commit();
		}catch (HibernateException e) {
			if(tx!=null) tx.rollback();
			e.printStackTrace();
		} finally{
			session.close();
		}
		return departmentID;
	}
	
	public void listDepartments(){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			List departments = session.createQuery("FROM Department").list();
			for(Iterator iterator1 = departments.iterator();iterator1.hasNext();){
				Department department = (Department) iterator1.next();
				System.out.print("Department ID: " + department.getDepartmentId());
				System.out.print("Department Name : " + department.getDepartmentName());
				Set employees = department.getEmployees();
				for(Iterator iterator2= employees.iterator();iterator2.hasNext();){
					Employee employee = (Employee) iterator2.next();
					System.out.println("Employee Id : " + employee.getEmployeeId());
					System.out.println("Employee Name : " + employee.getEmployeeName());
					System.out.println("Employee Band : " + employee.getEmployeeBand());
				}
		    }
			tx.commit();
	    } catch (HibernateException e) {
	    	if(tx!=null) tx.rollback();
	    	e.printStackTrace();
	    } finally {
	    	session.close();
	    }
	}
	
	public void updateDepartment(String depId,String depname){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			Department department = (Department) session.get(Department.class, depId);
			department.setDepartmentName(depname);
			session.update(department);
			tx.commit();
		} catch(HibernateException e){
			if(tx!=null) tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void deleteDepartment(String depId){
		Session session = factory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			Department department = (Department) session.get(Department.class, depId);
			session.delete(department);
			tx.commit();
		} catch (HibernateException e){
			if(tx!=null) tx.rollback();
			e.printStackTrace();
		} finally{
			session.close();
		}
	}
}		
		
