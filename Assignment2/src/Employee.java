
public class Employee {
	
	private String EmployeeId;
	private String EmployeeName;
	private String EmployeeBand;
	private String DepartmentId;
	
	public Employee() {}

	public Employee(String employeeId, String employeeName, String employeeBand, String departmentId) {
		EmployeeId = employeeId;
		EmployeeName = employeeName;
		EmployeeBand = employeeBand;
		DepartmentId = departmentId;
	}

	public String getDepartmentId() {
		return DepartmentId;
	}

	public void setDepartmentId(String departmentId) {
		DepartmentId = departmentId;
	}

	public String getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(String employeeId) {
		EmployeeId = employeeId;
	}
	public String getEmployeeName() {
		return EmployeeName;
	}
	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}
	public String getEmployeeBand() {
		return EmployeeBand;
	}
	public void setEmployeeBand(String employeeBand) {
		EmployeeBand = employeeBand;
	}

	public boolean equals(Object obj){
		if(obj==null) return false;
		if(!this.getClass().equals(obj.getClass())) return false;
		
		Employee obj2 = (Employee)obj;
		if((this.EmployeeId == obj2.getEmployeeId()) && (this.EmployeeName.equals(obj2.getEmployeeName()))){
			return true;
		}
		return false;
	}
	
	public int hashCode() {
		int tmp=0;
		tmp = (EmployeeId + EmployeeName).hashCode();
		return tmp;
	}
}
