
public class Employee {
	
	private String EmployeeId;
	private String EmployeeName;
	private String EmployeeBand;
	
	public Employee() {}
	
	public Employee(String employeeId, String employeeName, String employeeBand) {
		EmployeeId = employeeId;
		EmployeeName = employeeName;
		EmployeeBand = employeeBand;
	}

	public String getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(String employeeId) {
		EmployeeId = employeeId;
	}
	public String getEmployeeName() {
		return EmployeeName;
	}
	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}
	public String getEmployeeBand() {
		return EmployeeBand;
	}
	public void setEmployeeBand(String employeeBand) {
		EmployeeBand = employeeBand;
	}
}
