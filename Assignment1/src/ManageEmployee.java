
import java.util.List;
import java.util.Date;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManageEmployee {

	private static SessionFactory factory;
	public static void main(String[] args) {
		
		try{
			factory = new Configuration().configure().buildSessionFactory();
		} catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		
		ManageEmployee ME = new ManageEmployee();
		
		String empID1 = ME.addEmployee("he388719","Hemanth","B1");
		String empID2 = ME.addEmployee("hj637328","whfiebhjs","B2");
		String empId3 = ME.addEmployee("ks366789","uqwhjsa","C1");
		
		ME.listEmployees();
		
		ME.updateEmployee("hj637328","B3");
		
		ME.deleteEmployee("ks366789");
		
		ME.listEmployees();
	}
		
		public String addEmployee(String empid, String empname,String empband){
			
			Session session = factory.openSession();
			Transaction tx = null;
			String employeeID = null;
			try{
				tx = session.beginTransaction();
				Employee employee = new Employee(empid,empname,empband);
				employeeID = (String) session.save(employee);
				tx.commit();
			} catch(HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return employeeID;
		}
		
		public void listEmployees(){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				List employees = session.createQuery("FROM Employee").list();
				for(Iterator iterator = employees.iterator(); iterator.hasNext();){
					Employee employee = (Employee) iterator.next();
					System.out.println("Employee Id : " + employee.getEmployeeId());
					System.out.println("Employee Name : " + employee.getEmployeeName());
					System.out.println("Employee Band : " + employee.getEmployeeBand());
				}
				tx.commit();
			} catch(HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
		
		public void updateEmployee(String empid,String empband){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				Employee employee = (Employee)session.get(Employee.class, empid);
				employee.setEmployeeBand(empband);
				   session.update(employee);
				tx.commit();
			} catch (HibernateException e) {
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
		
		public void deleteEmployee(String EmployeeID){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				Employee employee = (Employee)session.get(Employee.class, EmployeeID);
				session.delete(employee);
				tx.commit();			
			} catch (HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}

}
