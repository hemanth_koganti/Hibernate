
import java.util.*;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ManageEmployee {

	private static SessionFactory factory;
	public static void main(String[] args) {
		
		try{
			factory = new Configuration().configure().buildSessionFactory();
		} catch(Throwable ex){
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		
		ManageEmployee ME = new ManageEmployee();
		
		Passport passport1 = ME.addPassport("HA87657627","he388719","Hemanth");
		
		String empID1 = ME.addEmployee("he388719", "Hemanth", "B1", passport1);
		
		Passport passport2 = ME.addPassport("JH67E456","kj56789","jhgtjjk");
		
		String empID2 = ME.addEmployee("kj56789", "jhgtjjk", "B3", passport2);
		
		Passport passport3 = ME.addPassport("KS789I87","ks366789","ksghdte");
		
		String empID3 = ME.addEmployee("ks366789","ksghdte","B2",passport3);
		
		ME.listEmployees();
		
		ME.updateEmployee(empID2,"B2");
		
		ME.deleteEmployee(empID3);
		
		ME.listEmployees();
		
	}
	
	
	    public Passport addPassport(String psprtno,String empid,String psprtname){
	    	Session session = factory.openSession();
	    	Transaction tx = null;
	    	String passportId = null;
	    	Passport passport = null;
	    	try{
	    		tx = session.beginTransaction();
	    		passport = new Passport(psprtno,empid,psprtname);
	    		passportId = (String) session.save(passport);
	    		tx.commit();
	    	} catch (HibernateException e) {
	    		if(tx!=null) tx.rollback();
	    		e.printStackTrace();
	    	} finally {
	    		session.close();
	    	}
	    	return passport;
	    }
		
		public String addEmployee(String empid, String empname,String empband,Passport psprt){
			
			Session session = factory.openSession();
			Transaction tx = null;
			String employeeID = null;
			try{
				tx = session.beginTransaction();
				Employee employee = new Employee(empid,empname,empband,psprt);
				employeeID = (String) session.save(employee);
				tx.commit();
			} catch(HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
			return employeeID;
		}
		
		public void listEmployees(){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				List employees = session.createQuery("FROM Employee").list();
				for(Iterator iterator = employees.iterator(); iterator.hasNext();){
					Employee employee = (Employee) iterator.next();
					System.out.println("Employee Id : " + employee.getEmployeeId());
					System.out.println("Employee Name : " + employee.getEmployeeName());
					System.out.println("Employee Band : " + employee.getEmployeeBand());
					Passport p1 = employee.getPassport();
					System.out.println("Passport No : " + p1.getPassportNo());
				}
				tx.commit();
			} catch(HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
		
		public void updateEmployee(String empid,String empband){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				Employee employee = (Employee)session.get(Employee.class, empid);
				employee.setEmployeeBand(empband);
				   session.update(employee);
				tx.commit();
			} catch (HibernateException e) {
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
		
		public void deleteEmployee(String EmployeeID){
			Session session = factory.openSession();
			Transaction tx = null;
			
			try{
				tx = session.beginTransaction();
				Employee employee = (Employee)session.get(Employee.class, EmployeeID);
				session.delete(employee);
				tx.commit();			
			} catch (HibernateException e){
				if(tx!=null) tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}

}
		
		
