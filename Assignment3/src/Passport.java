
public class Passport {
	private String PassportNo;
	private String EmployeeId;
	private String PassportName;
	
	
	
	public Passport(String passportNo, String employeeId, String passportName) {
		PassportNo = passportNo;
		EmployeeId = employeeId;
		PassportName = passportName;
	}

	public String getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(String employeeId) {
		EmployeeId = employeeId;
	}

	public String getPassportName() {
		return PassportName;
	}

	public void setPassportName(String passportName) {
		PassportName = passportName;
	}

	public Passport() {
	}

	public String getPassportNo() {
		return PassportNo;
	}

	public void setPassportNo(String passportNo) {
		PassportNo = passportNo;
	}
	
}
