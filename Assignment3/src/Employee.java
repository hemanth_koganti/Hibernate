
public class Employee {
	
	private String EmployeeId;
	private String EmployeeName;
	private String EmployeeBand;
	private Passport passport;
	
	public Employee() {}
	
	public Employee(String employeeId, String employeeName, String employeeBand, Passport passport) {
		this.EmployeeId = employeeId;
		this.EmployeeName = employeeName;
		this.EmployeeBand = employeeBand;
		this.passport = passport;
	}
	
	public String getEmployeeId() {
		return EmployeeId;
	}
	public void setEmployeeId(String employeeId) {
		EmployeeId = employeeId;
	}
	public String getEmployeeName() {
		return EmployeeName;
	}
	public void setEmployeeName(String employeeName) {
		EmployeeName = employeeName;
	}
	public String getEmployeeBand() {
		return EmployeeBand;
	}
	public void setEmployeeBand(String employeeBand) {
		EmployeeBand = employeeBand;
	}
	public Passport getPassport() {
		return passport;
	}
	public void setPassport(Passport passport) {
		this.passport = passport;
	}
	
	
}
